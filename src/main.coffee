{startDrag, dragGroup, stopDrag} = require './dragPageGroup'

pack.add 'imgs/paru-home.webp'
pack.add 'imgs/paru-home.png'
pack.add 'imgs/paru-vegan.webp'
pack.add 'imgs/paru-vegan.png'
pack.add 'imgs/paru-nutri-colheita.webp'
pack.add 'imgs/paru-nutri-colheita.png'
pack.add 'imgs/paru-nutri-uniforme.webp'
pack.add 'imgs/paru-nutri-uniforme.png'
pack.add 'imgs/paru-atleta.webp'
pack.add 'imgs/paru-atleta.png'

yeahWebP = ->
    console.log 'Yeah WebP :-)'
    document.body.classList.remove('no-webp')
    document.body.classList.add('yeah-webp')
    document.querySelectorAll('.page img').forEach (img)->
        webp = pack.get img.dataset.src + '.webp'
        img.src = webp if webp

noWebP = ->
    console.log 'No WebP :-('
    document.body.classList.remove('yeah-webp')
    document.body.classList.add('no-webp')
    document.querySelectorAll('.page img').forEach (img)->
        png = pack.get img.dataset.src + '.png'
        img.src = png if png

window.addEventListener 'DOMContentLoaded', ->
    for groupBox in document.querySelectorAll '.page-group-box'
        groupBox.curPage = groupBox.querySelector('.page')
        groupBox.numPages = groupBox.querySelectorAll('.page').length
        groupBox.addEventListener 'touchstart', startDrag.bind groupBox
        groupBox.addEventListener 'touchmove',  dragGroup.bind groupBox
        groupBox.addEventListener 'touchend',   stopDrag.bind  groupBox
        groupBox.addEventListener 'touchcancel',stopDrag.bind  groupBox
    require './paginate'

    testWebPTimeout = setTimeout noWebP, 2000
    img = new Image
    img.onload = ->
        clearTimeout testWebPTimeout
        if img.height > 0 then do yeahWebP else do noWebP
    img.onerror = ->
        clearTimeout testWebPTimeout
        do noWebP
    img.src = pack.get 'imgs/paru-home.webp'

    document.querySelector('#obras .content button').addEventListener 'click', ->
        document.querySelector('#obras').classList.add 'closed'

    window.construcao = ->
        document.querySelector('#obras').classList.remove 'closed'

proportions = [
    [ 0.500, '1por2'  ]
    [ 0.666, '2por3'  ]
    [ 0.770, '10por13']
    [ 1.000, '1por1'  ]
    [ 1.250, '5por4'  ]
    [ 1.333, '4por3'  ]
    [ 1.500, '3por2'  ]
    [ 1.600, '16por10']
    [ 2.000, '2por1'  ]
]

updateWindowPropClasses = ->
    classList = document.documentElement.classList
    prop = window.innerWidth / window.innerHeight
    classList.remove c for c in classList when c and c.match /^prop-/
    for [val, nome] in proportions
        if val > prop
            classList.add "prop-LT-#{nome}"
        else
            classList.add "prop-GT-#{nome}"
    if 0.7 < prop < 0.8
        classList.add "prop-tablet"
    else
        classList.remove "prop-tablet"

setTimeout updateWindowPropClasses, 1
#setInterval updateWindowPropClasses, 1000
window.addEventListener 'resize', updateWindowPropClasses
window.addEventListener 'resize', -> setTimeout updateWindowPropClasses, 500

