{ getTitle, timeout } = require './util'
{ dockGroupTo, stopPageGroupAnimation } = require './dragPageGroup'

updateLocationTimeout = null

goTo = (ev)->
    do ev.preventDefault
    return unless @pointTo
    groupBox = @pointTo.parentElement.parentElement
    stopPageGroupAnimation groupBox
    console.log 'Go to ' + @pointTo.id, @pointTo.offsetTop
    window.scrollTo left: 0, top: @pointTo.offsetTop, behavior: 'smooth'
    unless groupBox.classList.contains('page-group-box')
        console.log @pointTo.id + ' is not in a group.'
        return null
    posX = 0
    curEl = @pointTo
    while curEl.previousElementSibling
        curEl = curEl.previousElementSibling
        posX += curEl.clientWidth
    ini = do Date.now
    clearTimeout updateLocationTimeout if updateLocationTimeout
    dockGroupTo groupBox, posX, (err)=>
        if err
            console.error err
        else
            delay = 1200 - (Date.now() - ini)
            delay = 1 if delay < 1
            #updateLocationTimeout = setTimeout (=> document.location.href = '#' + @pointTo.id), delay
            updateLocationTimeout = timeout delay/1000, history.pushState {}, getTitle(@pointTo), '#' + @pointTo.id

for link in document.querySelectorAll 'nav a'
    link.pointTo = document.getElementById link.href.split('#')[1]
    console.log 'Menu link to ' + link.pointTo.id
    link.addEventListener 'click', goTo

# Build group pagination buttons:
for page in document.querySelectorAll '.page-group .page'
    btPrev = document.createElement 'a'
    btNext = document.createElement 'a'
    btPrev.className = 'pagination prev'
    btNext.className = 'pagination next'
    btPrev.pointTo = page.previousElementSibling
    btNext.pointTo = page.nextElementSibling
    btPrev.href = if btPrev.pointTo then '#' + btPrev.pointTo.id else ''
    btNext.href = if btNext.pointTo then '#' + btNext.pointTo.id else ''
    btPrev.page = btNext.page = page
    btPrev.addEventListener 'click', goTo
    btNext.addEventListener 'click', goTo
    page.appendChild btPrev
    page.appendChild btNext


