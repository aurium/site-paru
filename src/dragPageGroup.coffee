{ getTitle, timeout } = require './util'
updateLocationTimeout = null

exports.stopPageGroupAnimation = stopPageGroupAnimation = (groupBox)->
    clearTimeout updateLocationTimeout if updateLocationTimeout
    clearInterval groupBox.inertiaInterval if groupBox.inertiaInterval
    dockFinish groupBox, Error 'Force finish'

exports.startDrag = startDrag = (ev)->
    console.log 'Start drag ' + @className
    if ev.touches.length is 1
        touch = ev.touches[0]
        @dragGroupFrom = { pageX: touch.pageX, scrollLeft: @scrollLeft }
        stopPageGroupAnimation this

exports.dragGroup = dragGroup = (ev)->
    if @dragGroupFrom and ev.touches.length is 1
        touch = ev.touches[0]
        lastScrollLeft = @scrollLeft
        @scrollLeft = @dragGroupFrom.scrollLeft + @dragGroupFrom.pageX - touch.pageX
        @inertia = @scrollLeft - lastScrollLeft
        @inertiaWay = Math.sign @inertia
        @inertia = Math.abs @inertia

exports.stopDrag = stopDrag = (ev)->
    console.log 'Stop drag ' + @className
    @dragGroupFrom = false if ev.button is 0
    @inertiaInterval = setInterval dragInertia.bind(@), 33

dragInertia = ->
    if @inertia < 5
        clearInterval @inertiaInterval
        pageWidth = @querySelector('.page').clientWidth
        pageIndex = Math.round @scrollLeft / pageWidth
        dest = @querySelectorAll('.page')[pageIndex]
        destX = pageWidth * pageIndex
        ini = do Date.now
        dockGroupTo this, destX, (err)=>
            if err
                console.error err
            else
                delay = 1000 - (Date.now() - ini)
                delay = 1 if delay < 1
                #updateLocationTimeout = setTimeout (=> document.location.href = '#' + dest.id), delay
                updateLocationTimeout = timeout delay/1000, history.pushState {}, getTitle(dest), '#' + dest.id
    else
        @inertia -= 5
        @scrollLeft += @inertia * @inertiaWay

exports.dockGroupTo = dockGroupTo = (groupBox, destX, dockEndCallback)->
    console.log "INI Dock #{groupBox.className} to #{destX}"
    stopPageGroupAnimation groupBox
    groupBox.inertia = 10
    groupBox.dockDestX = destX
    groupBox.dockEndCallback = dockEndCallback
    groupBox.dockInterval = setInterval dock.bind(groupBox), 33

dock = ->
    @scrollLeft += if @scrollLeft < @dockDestX then @inertia else -@inertia
    if Math.abs(@scrollLeft - @dockDestX) < @inertia
        @scrollLeft = @dockDestX
        dockFinish this
    else
        dist = Math.abs @scrollLeft - @dockDestX
        if @inertia < dist/5
            @inertia += 5
        else if @inertia > dist/5 and @inertia > 10
            @inertia -= 8

dockFinish = (groupBox, err)->
    return null unless groupBox.dockInterval
    pages = groupBox.querySelectorAll '.page'
    pageIndex = Math.round groupBox.scrollLeft / pages[0].clientWidth
    groupBox.curPage = pages[pageIndex]
    console.log "END Dock #{groupBox.className} to #{groupBox.curPage.id} #{if err then 'ERR: '+err.message else ''}"
    clearInterval groupBox.dockInterval
    groupBox.dockInterval = null
    groupBox.dockEndCallback err, groupBox if 'function' is typeof groupBox.dockEndCallback
    groupBox.dockEndCallback = null

rootItens = []
window.addEventListener 'DOMContentLoaded', ->
    rootItens = document.querySelectorAll('body > .page, body > .page-group-box')

adjustVertScroll = do ->
    adjustVertScrollTimeout = null
    pageDest = null
    vertDockDest = 0
    pageHeight = 0
    w = window
    w.addEventListener 'scroll', (ev)->
        return if rootItens.length is 0
        pageHeight = rootItens[0].clientHeight
        pageIndex = Math.round w.scrollY / pageHeight
        rootDest = rootItens[pageIndex]
        pageDest = if rootDest.classList.contains('page') then rootDest else rootDest.curPage
        vertDockDest = pageHeight * pageIndex
        clearTimeout adjustVertScrollTimeout if adjustVertScrollTimeout
        adjustVertScrollTimeout = setTimeout adjustVertScroll, 500
    ->
        stopPageGroupAnimation {}
        w.scrollTo left: 0, top: vertDockDest, behavior: 'smooth'
        #updateLocationTimeout = setTimeout (=> document.location.href = '#' + pageDest.id), 400
        updateLocationTimeout = timeout .4, history.pushState {}, getTitle(pageDest), '#' + pageDest.id

