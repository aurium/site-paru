exports.timeout = (secs, cb)-> setTimeout cb, secs*1000

exports.getTitle = (el)->
    title = el.querySelector 'h1,h2'
    return title.innerText if title
    return el.innerText.split(String.fromCharCode 10)[0]
